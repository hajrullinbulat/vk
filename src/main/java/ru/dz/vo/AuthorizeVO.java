package ru.dz.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthorizeVO {
    private String accessToken;
    private String expires;
    private String userId;

    //// TODO: 16.08.2016 необходимо сделать проверку
    public boolean mapAuthorizeDTO(String response) {
        Pattern pattern = Pattern.compile("access_token=(.+?)&expires_in=(.+?)&user_id=(.*)");
        Matcher matcher = pattern.matcher(response);
        while (matcher.find()) {
            this.accessToken = matcher.group(1);
            this.expires = matcher.group(2);
            this.userId = matcher.group(3);
        }
        return accessToken != null && expires != null && userId != null;
    }
}
