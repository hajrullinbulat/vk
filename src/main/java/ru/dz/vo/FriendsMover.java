package ru.dz.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.dz.dto.User;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FriendsMover {
    private List<User> friends;
    private int index;

    public User getCurrentUser() {
        return friends.get(index);
    }

    public int getSize() {
        return friends.size();
    }

    public void upIndex() {
        index = index + 1;
    }

    public int newFriends(List<User> friends) {
        this.friends = friends;
        index = 0;
        return friends.size();
    }
}

