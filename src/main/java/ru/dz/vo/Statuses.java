package ru.dz.vo;

import lombok.Data;

@Data
public class Statuses {
    boolean iCanContinueMessaging;

    public Statuses() {
        iCanContinueMessaging = true;
    }
}
