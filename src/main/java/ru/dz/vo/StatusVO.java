package ru.dz.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.http.client.utils.URIBuilder;
import ru.dz.util.Status;
import ru.dz.dto.Error;
import ru.dz.wrapper.JSONWrapper;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatusVO {
    Status status;
    JSONWrapper jsonWrapper;
    Error error;
    URIBuilder builder;
}
