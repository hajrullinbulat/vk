package ru.dz.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "error_code",
        "error_msg",
        "request_params",
        "captcha_img",
        "captcha_sid"
})
public class Error {

    @JsonProperty("error_code")
    public int errorCode;
    @JsonProperty("error_msg")
    public String errorMsg;
    @JsonProperty("captcha_img")
    public String captchaImage;
    @JsonProperty("captcha_sid")
    public String captchaId;

    @JsonProperty("request_params")
    public List<RequestParam> requestParams = new ArrayList<RequestParam>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
