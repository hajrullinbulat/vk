package ru.dz.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import ru.dz.Thread.AuthorizeThread;
import ru.dz.Thread.FriendsGetThread;
import ru.dz.Thread.MessagesSender;

//// TODO: 22.08.2016 DAGGER 2
public class Controller {
    @FXML
    public Text access_field;
    @FXML
    public Text friends_field;
    @FXML
    public Text message_field;
    @FXML
    public TextArea message_textarea;
    @FXML
    public TextField attaches;
    @FXML
    public VBox logs;
    @FXML
    public VBox users;

    public Controller() {
    }

    @FXML
    protected void handleAuthorizeAction(MouseEvent event) {
        final AuthorizeThread authorizeThread = new AuthorizeThread(logs);
        authorizeThread.run();
    }

    @FXML
    public void getFriends(MouseEvent mouseEvent) {
        FriendsGetThread friendsGetThread = new FriendsGetThread(friends_field, logs, users);
        friendsGetThread.run();
    }

    @FXML
    public void sendMessageToAll(MouseEvent mouseEvent) {
        MessagesSender messagesSenderThread = new MessagesSender(logs, attaches, message_textarea);
        Thread thread = new Thread(messagesSenderThread);
        thread.setDaemon(true);
        thread.start();
    }

}

