package ru.dz.Thread;

import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import ru.dz.dto.User;
import ru.dz.util.Status;
import ru.dz.vo.StatusVO;

import java.util.ArrayList;
import java.util.List;

import static ru.dz.Thread.Core.*;

public class FriendsGetThread extends Task<Void> {
    private Text friends_field;
    private VBox users;
    private VBox logs;

    public FriendsGetThread(Text friends_field, VBox logs, VBox users) {
        this.friends_field = friends_field;
        this.logs = logs;
        this.users = users;
    }

    @Override
    protected Void call() {
        StatusVO friends = getFriends();
        if (friends.getStatus().equals(Status.SUCCESS)) {
            friends_field.setText("Кол-во друзей:"
                    + friendsMover.newFriends(friends.getJsonWrapper().response.users));
            Core.log("Друзья были найдены!", logs);
            setUsersInToUi();
        } else {
            String errorMsg = friends.getError().errorMsg;
            callErrorPopup(errorMsg);
            log("Ошибка: " + errorMsg, logs);
        }
        return null;
    }

    private void setUsersInToUi() {
        List<Text> texts = new ArrayList<>();
        List<User> friends = friendsMover.getFriends();
        for (int i = 0; i < friends.size(); i++) {
            Text text = new Text(i + ". " + friends.get(i).lastName + " " + friends.get(i).firstName);
            final int finalI = i;
            text.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    Core.friendsMover.setIndex(finalI);
                }
            });
            texts.add(text);
        }
        users.getChildren().clear();
        users.getChildren().addAll(texts);
    }
}
