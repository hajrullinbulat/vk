package ru.dz.Thread;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import ru.dz.vo.StatusVO;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import static ru.dz.Thread.Core.sendMessage;

public class CaptchaPopup implements Callable<StatusVO> {

    private StatusVO statusVO;

    public CaptchaPopup(StatusVO statusVO) {
        this.statusVO = statusVO;
    }

    private StatusVO newStatus;

    @Override
    public StatusVO call() throws Exception {
        final Pane pane = new Pane();
        pane.setPrefSize(226, 152);
        WebView web = new WebView();
        web.setPrefSize(130, 50);
        web.setLayoutX(48);
        web.setLayoutY(14);
        web.getEngine().load(statusVO.getError().captchaImage);
        Button button = new Button("Отправить");
        button.setLayoutX(76);
        button.setLayoutY(113);
        final TextField textField = new TextField();
        textField.setPrefSize(130, 23);
        textField.setLayoutY(76);
        textField.setLayoutX(48);
        pane.getChildren().add(web);
        pane.getChildren().add(button);
        pane.getChildren().add(textField);
        final Stage stage = new Stage(StageStyle.DECORATED);
        button.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.close();
                List<NameValuePair> captchaParams = new ArrayList<>();
                captchaParams.add(new BasicNameValuePair("captcha_sid", statusVO.getError().captchaId));
                captchaParams.add(new BasicNameValuePair("captcha_key", textField.getText()));

                newStatus = sendMessage(statusVO.getBuilder(), captchaParams);
            }
        });
        stage.setScene(new Scene(pane));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.toFront();
        stage.showAndWait();
        return newStatus;
    }
}
