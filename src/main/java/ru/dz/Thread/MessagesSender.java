package ru.dz.Thread;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import ru.dz.util.Status;
import ru.dz.vo.StatusVO;

import java.util.concurrent.FutureTask;

import static ru.dz.Thread.Core.*;

public class MessagesSender extends Task<Void> {
    private TextArea message;
    private TextField attaches;
    private VBox logs;

    public MessagesSender(VBox logs, TextField attaches, TextArea message) {
        this.logs = logs;
        this.attaches = attaches;
        this.message = message;
    }

    @Override
    protected Void call() throws Exception {
        StatusVO statusVO = null;
        while (friendsMover.getIndex() < friendsMover.getSize()) {
            if (statusVO != null && statusVO.getStatus().equals(Status.SUCCESS)) {
                log("Сообщение №"
                                + friendsMover.getIndex()
                                + " из " + friendsMover.getSize()
                                + " успешно отправлено! (" + friendsMover.getCurrentUser().lastName + " " + friendsMover.getCurrentUser().firstName + ")"
                        , logs);
                friendsMover.upIndex();
                statusVO = prepareToSendMessage(friendsMover.getCurrentUser(), attaches, message);
            } else if (statusVO == null) {
                statusVO = prepareToSendMessage(friendsMover.getCurrentUser(), attaches, message);
                log("Начало массовой рассылки", logs);
            } else if (statusVO.getStatus().equals(Status.CAPTCHA)) {
                log("Captcha!", logs);
                FutureTask<StatusVO> futureTask = new FutureTask<>(
                        new CaptchaPopup(statusVO)
                );
                Platform.runLater(futureTask);
                statusVO = futureTask.get();
            } else if (statusVO.getStatus().equals(Status.PERMISSIONS)) {
                log("Данному пользователю нельзя написать через массовую рассылку. ("
                        + friendsMover.getCurrentUser().lastName
                        + " "
                        + friendsMover.getCurrentUser().firstName
                        + ")", logs);
                friendsMover.upIndex();
                statusVO = prepareToSendMessage(friendsMover.getCurrentUser(), attaches, message);
            } else {
                String errorMsg = statusVO.getError().errorMsg;
                callErrorPopup(errorMsg);
                log("Ошибка: " + errorMsg, logs);
                break;
            }
            Thread.sleep(3500);
        }
        friendsMover.setIndex(0);
        log("Рассылка закончена!", logs);
        return null;
    }


}
