package ru.dz.Thread;

import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import ru.dz.util.Constants;
import ru.dz.util.LogUiWrapper;

import static ru.dz.Thread.Core.*;

public class AuthorizeThread extends Task<Void> {
    private VBox logs;

    public AuthorizeThread(VBox logs) {
        this.logs = logs;
    }

    @Override
    protected Void call() throws Exception {
        final WebView webView = new WebView();
        final WebEngine engine = webView.getEngine();
        engine.load(Constants.AUTHORIZE);
        final Stage stage = new Stage(StageStyle.DECORATED);
        stage.setScene(new Scene(webView, 500, 400));
        stage.setTitle("Авторизация");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.toFront();
        stage.show();
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                if (isAuthorized(engine.getLocation())) {
                    log("Авторизация прошла успешно!", logs);
                } else {
                    log("Авторизация была прервана!", logs);
                    stage.close();
                    try {
                        call();
                    } catch (Exception e) {
                        callErrorPopup(e.getMessage());
                        log("Ошибка: " + e.getMessage(), logs);
                    }
                }
            }
        });
        return null;
    }
}
