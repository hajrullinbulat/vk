package ru.dz.Thread;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import ru.dz.dto.Error;
import ru.dz.dto.User;
import ru.dz.util.Constants;
import ru.dz.util.MethodType;
import ru.dz.util.Status;
import ru.dz.vo.AuthorizeVO;
import ru.dz.vo.FriendsMover;
import ru.dz.vo.StatusVO;
import ru.dz.wrapper.ErrorWrapper;
import ru.dz.wrapper.JSONWrapper;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static ru.dz.util.MethodType.GET_FRIENDS;
import static ru.dz.util.MethodType.SEND_MESSAGE;

public class Core {
    public static AuthorizeVO authorizeVO = new AuthorizeVO();
    public static FriendsMover friendsMover = new FriendsMover();
    private static List<Text> logs;

    static void callErrorPopup(final String message) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Stage stage = new Stage(StageStyle.DECORATED);
                TextArea textArea = new TextArea(message);
                textArea.setEditable(false);
                textArea.setWrapText(true);
                textArea.setPrefSize(400, 300);
                Pane pane = new Pane(textArea);
                pane.setPrefSize(400, 300);
                stage.setScene(new Scene(pane));
                stage.setTitle("ОШИБКА");
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.toFront();
                stage.show();
            }
        });

    }

    static boolean isAuthorized(String response) {
        return authorizeVO.mapAuthorizeDTO(response);
    }

    public static URIBuilder prepareBuilder() {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("access_token", authorizeVO.getAccessToken()));
        params.add(new BasicNameValuePair("v", Constants.VK_API_VERSION));
        return new URIBuilder()
                .setScheme("https")
                .setHost(Constants.HOST)
                .setParameters(params);
    }

    public static StatusVO prepareToSendMessage(User user, TextField attaches, TextArea message) {
        URIBuilder builder = prepareBuilder();
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("user_id", String.valueOf(user.id)));
        params.add(new BasicNameValuePair("random_id", String.valueOf(new Random().nextInt(10000) + 1)));
        params.add(new BasicNameValuePair("message", user.lastName + " " + user.firstName + ", " + message.getText()));

        String text = attaches.getText();
        if (text != null) {
            params.add(new BasicNameValuePair("attachment", text));
        }

        builder
                .addParameters(params)
                .setPath(Constants.PATH + SEND_MESSAGE.toString());
        return sendMessage(builder, null);
    }

    public static StatusVO getFriends() {
        URIBuilder builder = prepareBuilder();
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("order", "name"));
        params.add(new BasicNameValuePair("fields", "nickname"));

        builder
                .addParameters(params)
                .setPath(Constants.PATH + GET_FRIENDS.toString());

        try (CloseableHttpClient client = HttpClientBuilder.create().build()) {
            CloseableHttpResponse response = client.execute(new HttpGet(builder.build()));
            return checkJsonResp(EntityUtils.toString(response.getEntity(), "UTF-8"), builder, MethodType.GET_FRIENDS);
        } catch (IOException | URISyntaxException e) {
            callErrorPopup(e.getMessage());
            return new StatusVO(Status.UNKNOWN, null, null, null);
        }
    }

    public static StatusVO sendMessage(URIBuilder builder, List<NameValuePair> captcha) {
        try (CloseableHttpClient client = HttpClientBuilder.create().build()) {
            System.out.println(builder.build());
            System.out.println(builder.build());
            System.out.println(builder.build());
            if (captcha != null)
                builder.addParameters(captcha);
            CloseableHttpResponse execute = client.execute(new HttpGet(builder.build()));
            return checkJsonResp(EntityUtils.toString(execute.getEntity(), "UTF-8"), builder, MethodType.SEND_MESSAGE);
        } catch (IOException | URISyntaxException e) {
            callErrorPopup(e.getMessage());
        }
        return null;
    }


    public static StatusVO checkJsonResp(String jsonResp, URIBuilder builder, MethodType type) throws IOException {
        ObjectMapper jsonMapper = new ObjectMapper();
        ErrorWrapper errorWrapper = jsonMapper.readValue(jsonResp, ErrorWrapper.class);
        Error error = errorWrapper.error;
        if (error != null)
            switch (error.errorCode) {
                case 14:
                    return new StatusVO(Status.CAPTCHA, null, error, builder);
                case 7:
                    return new StatusVO(Status.PERMISSIONS, null, error, builder);
                default:
                    return new StatusVO(Status.ERROR, null, error, builder);
            }
        else {
            switch (type) {
                case GET_FRIENDS:
                    return new StatusVO(Status.SUCCESS, jsonMapper.readValue(jsonResp, JSONWrapper.class), null, builder);
                default:
                    return new StatusVO(Status.SUCCESS, null, null, builder);
            }
        }
    }

    public static void log(final String text, final VBox logs) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                logs.getChildren().add(new Text(text));
            }
        });
    }


}
