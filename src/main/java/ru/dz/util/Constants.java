package ru.dz.util;


public interface Constants {
    String CLIENT_ID = "5384023";
    String VK_API_VERSION = "5.53";
    String AUTHORIZE = "https://oauth.vk.com/authorize?client_id=" + CLIENT_ID +
            "&display=popup" +
            "&redirect_uri=https://oauth.vk.com/blank.html" +
            "&scope=users,wall,messages,photos" +
            "&response_type=token" +
            "&v=" + VK_API_VERSION;
    String HOST = "api.vk.com";
    String PATH = "/method/";

}
