package ru.dz.util;

import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LogUiWrapper {
    VBox box;
    ScrollPane scrollPane;
}
