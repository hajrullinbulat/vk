package ru.dz.util;

public enum Status {
    CAPTCHA,
    ERROR,
    SUCCESS,
    UNKNOWN, PERMISSIONS;
}