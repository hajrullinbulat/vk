package ru.dz.util;

public enum MethodType {
    GET_FRIENDS("friends.get"),
    SEND_MESSAGE("messages.send");

    private final String text;

    MethodType(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
